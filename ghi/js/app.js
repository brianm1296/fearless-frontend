function createCard(name, description, pictureUrl, starts, ends, locationName) {
  starts = new Date(starts);
  ends = new Date(ends);
  const formattedStartDate = `${starts.getMonth() + 1}/${starts.getDate()}/${starts.getFullYear()}`;
  const formattedEndDate = `${ends.getMonth() + 1}/${ends.getDate()}/${ends.getFullYear()}`;


  return `
  <div class="card shadow mb-4">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer text-muted">
          ${formattedStartDate}-${formattedEndDate}
        </div>
      </div>
  </div>
  `;

  }


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
          alert("Bad response!")
      } else {
        const data = await response.json();

        let i = 0;


        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl)
          if (detailResponse.ok) {
              const details = await detailResponse.json();
              const name = details.conference.name
              const description = details.conference.description
              const pictureUrl = details.conference.location.picture_url
              const locationName = details.conference.location.name
              const starts = details.conference.starts
              const ends = details.conference.ends
              const html = createCard(name, description, pictureUrl, starts, ends, locationName)
              const column = document.querySelector(`#col_${i%3}`);
              column.innerHTML += html;
              i++
          } else {
              alert("Error occurred, could not get details!");


          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error(e);
      alert("Error was raised!");
    }

  });
